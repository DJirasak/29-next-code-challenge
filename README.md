# 29 Next Code Challenge

## Description

Write a program to solve this problem with python language. The application should find the shortest path between “Start Node” To “Goal Node”

## Solution
This Program implement with dijkstra's algorithm

## How to run program
```
python program.py
//or
python3 program.py
```