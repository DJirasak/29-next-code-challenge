def loadGraph(graphFile: str)->dict:
    graphData= {}
    if (graphFile == None or graphFile == "" ) : return graphData
    else: 
        f = open(graphFile, "r")
        dataLine = [i.strip("\n") for i in f]
        f.close()
        for i in range(len(dataLine)):
            data = dataLine[i].split(",")
            if data[0] not in graphData:
                graphData[data[0]] = {data[1]: int(data[2])}
                graphData[data[1]] = {data[0]: int(data[2])}
            else:
                graphData[data[0]] = {**graphData[data[0]], data[1]: int(data[2])}
                if data[1] not in graphData:
                    graphData[data[1]] = {data[0]: int(data[2])}
                else:
                    graphData[data[1]] = {**graphData[data[1]], data[0]: int(data[2])}
    return graphData

def findPath(source: str, target: str, graphFile: str = "graph.csv")->list:
    graphData = loadGraph(graphFile)
    numNode = len(graphData)
    success = False
    cost = 0
    path = []
    path.append(source)
    prevSrc = ""
    step = 0
    while not success:
        step += 1
        temp_cost = 10000000
        temp_path = ""
        if source not in graphData:
            success = True
        else:
            for i in graphData[source]:
                if i == target:
                    success = True
                    if(temp_cost == 10000000):
                        temp_cost = cost + graphData[source][i]
                        temp_path = i
                    else:
                        temp_cost = cost + graphData[source][i]
                        temp_path = i
                    break
                else:
                    if i != prevSrc:
                        if(temp_cost == 10000000 ):
                            temp_cost = cost + graphData[source][i]
                            temp_path = i
                        else:
                            if(cost + graphData[source][i] < temp_cost and i != path[-1]):
                                temp_cost = cost + graphData[source][i]
                                temp_path = i  
        cost = temp_cost
        path.append(temp_path)
        if temp_path == "" or step > numNode:
            return [0,[]]
        if success:
            return [cost, path]
        else:
            prevSrc = source
            source = temp_path

if __name__ == "__main__":
    graphFile = input("What is graph file name: ") #"graph.csv"
    startNode = input("What is start node: ")
    goalNode = input("What is goal node: ")
    result = findPath(startNode.upper(), goalNode.upper(), graphFile)
    path = '->'.join(map(str,result[1]))
    if result[0]==0:
        print("Path from",startNode.upper(),"to",goalNode.upper(),"is impossible.")
    else:
        print("Path from",startNode.upper(),"to",goalNode.upper(),"is",path,"and have cost "+str(result[0])+".")