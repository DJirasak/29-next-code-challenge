from program import loadGraph, findPath
import pytest

@pytest.mark.parametrize("sourceFile, expected",
	[
		(None, {}),
		("", {}),
		("graph.csv", {
            'A': {'B': 5, 'D': 3, 'E': 4}, 
            'B': {'A': 5, 'C': 4}, 
            'D': {'A': 3, 'G': 6}, 
            'E': {'A': 4, 'F': 6}, 
            'C': {'B': 4, 'G': 2}, 
            'F': {'E': 6, 'H': 5}, 
            'G': {'C': 2, 'D': 6, 'H': 3}, 
            'H': {'G': 3, 'F': 5}, 
            'I': {'0': 0}, 
            '0': {'I': 0}
            })
	])
def test_loadGraph(sourceFile, expected):
    assert loadGraph(sourceFile) == expected

@pytest.mark.parametrize("source, target, expected",
	[
		("I", "A", [0 ,[]]),
		("A", "I", [0 ,[]]),
		("X", "Y", [0 ,[]]),
		("Y", "Q", [0 ,[]]),
		("A", "B", [5 ,["A","B"]]),
		("B", "A", [5 ,["B","A"]]),
		("C", "F", [10 ,["C","G","H","F"]]),
		("F", "G", [8 ,["F","H","G"]]),
		("F", "C", [10 ,["F","H","G","C"]]),
		("A", "H", [12 ,["A","D","G","H"]]),
		("A", "H", [12 ,["A","D","G","H"]]),
	])
def test_findPath(source, target, expected):
    assert findPath(source, target) == expected